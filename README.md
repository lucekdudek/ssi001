# SSI 01
jQuery AJAX   

# Jak uruchomić
Potrzeba Python 3.5 oraz bibliotek z requirements.txt.  
Pythona można pobrać ze strony python.org.  
Żeby zainstalować biblioteki należy użyć komendy `pip install -r requirements.txt`.  
Aby uruchomić serwer `python main.py`.  

# Autor
Lucjan Dudek.  
