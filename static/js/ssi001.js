
$(document).ready(function(){

    $("#ssi001_submit").click(function(){

        var error = false

        // validation of email field
        if(!$("#name").val()){
            $("#name_error").text("email is required")
            error = true;
        }
        else if($("#name").val().indexOf("@")<0){
            $("#name_error").text("must be email")
            error = true;
        }else{
            $("#name_error").text(" ");
        }

        // validation of PIN field
        if(!$("#pin").val()){
            $("#pin_error").text("pin is required")
            error = true;
        }else if(!jQuery.isNumeric($("#pin").val())){
            $("#pin_error").text("pin must be number")
            error = true;
        }else{
            $("#pin_error").text(" ");
        }

        // stop function in the cas eof fail validation
        if(error){
            return false;
        }

        // AJAX - send data to server and get response (in JSON)
        $.getJSON('/_check_hello', {
            name: $("#name").val(),
            pin: $("#pin").val()
        }, function (data) {
            $("#ajax_result").text(data.result);
        });
        return false;
    });

});
