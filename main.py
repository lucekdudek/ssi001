
from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request

app = Flask(__name__)


@app.route("/_check_hello")
def check_hello():
    # fake data base
    pseudo_db = {
        "default": "pseudo dane z pseudo bazy danych"
    }
    # get data from client
    email = request.args.get('name', 0, type=str)
    pin = request.args.get('pin', 0, type=int)
    # return data to client
    return jsonify(result="{0} - {1}: {2}".format(email, pin, pseudo_db["default"]))


@app.route("/")
def hello():
    # render index (/) page
    return render_template('hello.html')

if __name__ == "__main__":
    app.run()
